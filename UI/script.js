"use strict";

let original = document.getElementById("subdirectory-and-attacks"); //the sub dir and attacks
// the function will clone the structure of subdirectory and attacks to add another dir
function addSubDir() {
  let clone = original.cloneNode(true); // "deep" clone
  //empty all the inputs to make the new dir without any data
  clone
    .querySelector(".subdirectory-configuration")
    .querySelector(".subdirectory").value = "";
  let attackOptions = clone
    .querySelector(".attack-options")
    .querySelectorAll(".attack-option-container");
  for (let i = 0; i < attackOptions.length; i++) {
    attackOptions[i].querySelector(".attack-checkbox").checked = false;
  }
  //add the new dir to the dir list
  original.parentNode.appendChild(clone);
}

//the function is demo to check the connect btn
function connect() {
  document.getElementById("demo").innerHTML = "Connect";
}
