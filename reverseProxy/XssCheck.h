#ifndef XSS_CHECK
#define XSS_CHECK

#include "IAttackCheck.h"
#include <string>
#include <fstream>
#include <vector>

class XssCheck : public IAttackCheck
{
public:
	XssCheck();
	virtual bool check(PDU& pack);

private:
	std::vector<std::string> m_codes;
};

#endif // !XSS_CHECK