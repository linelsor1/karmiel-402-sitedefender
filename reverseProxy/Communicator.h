#ifndef COMMUNICATOR
#define COMMUNICATOR

#include "Client.h"
#include "XssCheck.h"
#include "BruteforceCheck.h"
#include "SqliCheck.h"
#include "SqliteDatabase.h"
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <thread>

#define MAX_CONNECTIONS_PER_CLIENT 20

class Communicator {
public:
	Communicator(IPv4Address siteAddr, IPv4Address serverAddr) : 
		m_siteIP(siteAddr),
		m_siteDomain("passcard.xyz"), 
		m_serverIP(serverAddr),
		m_iface(NetworkInterface::default_interface()),
		m_xssCheck(),
		m_db()
	{
	}
	~Communicator();
	void startListening();

private:
	IPv4Address m_siteIP;
	std::string m_siteDomain;
	IPv4Address m_serverIP;

	std::vector<Client*> m_clients;

	XssCheck m_xssCheck;
	SqliCheck m_sqliCheck;

	NetworkInterface m_iface;

	SqliteDatabase m_db;

	bool connectClient(PDU& clientPack);
	void handleClient(Client* client);

	void snifferFromSite(Client* client);
	void snifferFromClient(Client* client);

	bool scanPacket(PDU& packToSend, Client* client);

	bool sendToClient(PDU& packToSend, Client* client);
	bool sendToSite(PDU& packToSend);

	void disconnectClient(Client* client);
	void disconnectAttacker(Client* client);

	std::string getUrlOfPacket(std::string payload);
	Sniffer configureSniffer(std::string filter);
	std::string packetPayloadToStr(RawPDU::payload_type payload);
};

#endif // !COMMUNICATOR
