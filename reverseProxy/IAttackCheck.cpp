#include "IAttackCheck.h"

/*
coverts the payload of the pack to string
input:
	RawPDU& pack- the packet with the payload
output:
	the payload as string
*/
std::string IAttackCheck::convertPayloadToStr(RawPDU& pack)
{
	int i = 0;
	std::string data = "";
	RawPDU::payload_type payload = pack.payload();
	for (i = 0; i < payload.size(); i++)
	{
		data += payload[i];
	}
	return data;
}