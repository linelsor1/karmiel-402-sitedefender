#ifndef SQLI_CHECK
#define SQLI_CHECK

#include "IAttackCheck.h"
#include <fstream>
#include <vector>

class SqliCheck : public IAttackCheck
{
public:
	SqliCheck();
	virtual bool check(PDU& pack);

private:
	std::vector<std::string> m_commands;
	std::vector<std::string> m_signs;

	bool isAlpha(std::string str);
	bool checkHelper(std::string data, std::vector<std::string> lines, bool alpha);
};

#endif // !SQLI_CHECK
