#include "BruteforceCheck.h"

//ctir for BruteforceCheck
BruteforceCheck::BruteforceCheck(std::vector<std::string> urls)
{
	int i = 0;
	for (i = 0; i < urls.size(); i++)
	{
		m_urlAndSusPacks.insert(std::pair<std::string, int>(urls[i], 0));
		m_urlAndAttack.insert(std::pair<std::string, bool>(urls[i], false));
	}
}

//returns if a bruteforce attack is happening, when the url isnt given
bool BruteforceCheck::check(PDU& pack)
{
	std::string data = convertPayloadToStr(pack.rfind_pdu<RawPDU>());
	std::string url = data.substr(data.find("passcard.xyz/"), data.find(" HTTP/1.1"));
	return check(pack, url);
}

//returns if a bruteforce attack is happening
bool BruteforceCheck::check(PDU& pack, std::string url)
{
	if (m_urlAndSusPacks[url] == 0) //this is the first packet that is checked in this url
	{
		std::thread t(&BruteforceCheck::countTime, this, url); //start the clock
		t.detach();
	}
	m_urlAndSusPacks[url]++; //increase the amount of packs of this url
	m_urlAndAttack[url] = m_urlAndSusPacks[url] > MAX_PACKS; //in case the time hasn't passed and the user reached the max packets
	return m_urlAndAttack[url];
}

/*
does a stopwatch of MAX_TIME milliseconds, then checks if the pack count reached MAX_PACKS
input:
	none
output:
	none
*/
void BruteforceCheck::countTime(std::string url)
{
	Sleep(MAX_TIME);
	m_urlAndAttack[url] = m_urlAndSusPacks[url] > MAX_PACKS;

	std::cout << "amount of requests in this url: " << m_urlAndSusPacks[url] << "\n";
	std::string res = m_urlAndAttack[url] ? "yes\n" : "no\n";
	std::cout << "is there a bf attack on url '" << url << "': " << res;

	m_urlAndSusPacks[url] = 0; //start counting again
}