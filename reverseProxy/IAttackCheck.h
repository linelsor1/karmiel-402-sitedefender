#ifndef IATTACK_CHECK
#define IATTACK_CHECK

#define TINS_STATIC

enum ATTACK_TYPES {BF};

#include <tins/tins.h>
#include <iostream>

using namespace Tins;

class IAttackCheck
{
public:
	virtual bool check(PDU& pack) = 0;

protected:
	virtual std::string convertPayloadToStr(RawPDU& pack);
};

#endif // !IATTACK_CHECK
