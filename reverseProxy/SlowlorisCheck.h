#pragma once
#include "IAttackCheck.h"
#include <thread>

#define WAIT_TIME 60000

class SlowlorisCheck : public IAttackCheck
{
private:
	bool isPackRecieved;
	bool isAttack;

public:
	SlowlorisCheck();

	bool check(PDU& pack);
	void countTime();
};