#include "Client.h"

//ctor for client
Client::Client(IPv4Address ip, int port, std::vector<std::string> urls)
{
	m_ip = ip;
	m_port = port;
	BruteforceCheck* bf = new BruteforceCheck(urls);
	SlowlorisCheck* sl = new SlowlorisCheck();
	m_attacks.push_back(bf);
	m_attacks.push_back(sl);
}

Client::~Client()
{
	int i = 0;
	for (i = 0; i < m_attacks.size(); i++)
	{
		delete(m_attacks[i]);
	}
}

//gets the ip of the client
IPv4Address Client::getIp() const
{
	return m_ip;
}

//gets the port of the client
int Client::getPort() const
{
	return m_port;
}

//gets the fin pack of the client
EthernetII Client::getFinPack() const
{
	return m_finPack;
}

//gets the attack scans of the client
std::vector<IAttackCheck*> Client::getAttackScans() const
{
	return m_attacks;
}

//set the fin pack
void Client::setFinPack(PDU& pack)
{
	TCP tcp(80, m_port);
	tcp.seq(pack.rfind_pdu<TCP>().seq());
	tcp.ack_seq(pack.rfind_pdu<TCP>().ack_seq());
	tcp.set_flag(TCP::ACK, 1);
	tcp.set_flag(TCP::FIN, 1);

	EthernetII eth(pack.rfind_pdu<EthernetII>().dst_addr(), pack.rfind_pdu<EthernetII>().src_addr());

	m_finPack = eth / IP(pack.rfind_pdu<IP>().dst_addr(), m_ip) / tcp;
}

//for the client map
bool Client::operator<(Client const& client) const
{
	return this->m_ip < client.getIp();
}