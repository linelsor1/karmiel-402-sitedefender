#ifndef SERVER
#define SERVER

#include "Communicator.h"

class Server {
public:
	Server() : m_communicator(IPv4Address("34.201.80.84"), IPv4Address("10.0.0.13"))
	{
	}
	void run();
private:
	Communicator m_communicator;
};

#endif // !SERVER
