#include "SqliCheck.h"

//ctor for sqliCheck
SqliCheck::SqliCheck()
{
	std::ifstream file("sqlCodes.txt");
	std::string line;

	while (std::getline(file, line)) //push the commands to vector
	{
		if (!isAlpha(line))
		{
			m_signs.push_back(line);
		}
		else
		{
			m_commands.push_back(line);
		}
	}
}

/*
this function will check for sqli attacks in the data of the packet
input:
	pack - the packet
output:
	bool - true if there are attcks detected
*/
bool SqliCheck::check(PDU& pack)
{
	std::string data = convertPayloadToStr(pack.rfind_pdu<RawPDU>());
	std::string line;

	if (data.find('"') != std::string::npos || data.find("'") != std::string::npos)
	{
		if (checkHelper(data, m_commands, true))
		{
			return true;
		}
		return checkHelper(data, m_signs, false);
	}
	return false;
}

/*
a helper for the check function, it is a template to check the vector
input:
	std::string data- the data of the packet
	std::vector<std::string> lines- the lines to check
	bool alpha- if it is the commands or the signs
output:
	bool- true if there are attcks detected
*/
bool SqliCheck::checkHelper(std::string data, std::vector<std::string> lines, bool alpha)
{
	std::string line;
	for (std::vector<std::string>::iterator it = lines.begin(); it != lines.end(); it++)
	{
		line = alpha ? " " + *it + " " : *it;
		//checks if there is a command or sign of sql
		if (data.find(line) != std::string::npos)
		{
			std::cout << "sql detected: " + line + "\n";
			return true;
		}
	}
	return false;
}

/*
checks if the string is lettrs only
input:
	std::string str- the string to check
output:
	if the string is lettrs only
*/
bool SqliCheck::isAlpha(std::string str)
{
	int i = 0;
	bool check = true;
	for (i = 0; i < str.length() && check; i++)
	{
		check = isalpha(str[i]);
	}
	return check;
}