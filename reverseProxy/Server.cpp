#include "Server.h"

//the proxy will start running and listen for users
void Server::run() 
{
	try
	{
		m_communicator.startListening();
	}
	catch (std::exception e)
	{
		std::cout << e.what() << "\n";
	}
}