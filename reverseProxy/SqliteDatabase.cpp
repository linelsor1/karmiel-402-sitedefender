#include "SqliteDatabase.h"

sqlite3* SqliteDatabase::m_db = nullptr;

//ctor for the db
SqliteDatabase::SqliteDatabase()
{
	if (!open())
	{
		throw std::exception("couldn't open the DB.\n");
	}
}

/*
opens a database, if it was not found the function initates a new db with tables
input:
	none
output:
	if the db was opened successfully
*/
bool SqliteDatabase::open()
{
	std::string dbFileName = "proxyDB.sqlite";
	std::string sqlStatement = "";
	char* errMsg = nullptr;

	std::vector<std::string> wrongs;

	int doesFileExist = _access(dbFileName.c_str(), 0);

	int res = sqlite3_open(dbFileName.c_str(), &m_db);
	if (res != SQLITE_OK)
	{
		m_db = nullptr;
		return false;
	}
	if (doesFileExist == -1)
	{
		sqlStatement = "CREATE TABLE AttackersInfo (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, IP TEXT NOT NULL);";
		res = sqlite3_exec(m_db, sqlStatement.c_str(), nullptr, nullptr, &errMsg);
		if (res != SQLITE_OK)
		{
			throw std::exception("Failed to create table Attackers");
			return false;
		}

		sqlStatement = "CREATE TABLE AttackTypes (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ATTACK_NAME TEXT NOT NULL);";
		res = sqlite3_exec(m_db, sqlStatement.c_str(), nullptr, nullptr, &errMsg);
		if (res != SQLITE_OK)
		{
			throw std::exception("Failed to create table Attackers");
			return false;
		}

		addAttack("Sql Injection");
		addAttack("XSS");
		addAttack("Bruteforce");

		sqlStatement = "CREATE TABLE ManagersInfo (USERNAME TEXT PRIMARY KEY NOT NULL, PASSWORD TEXT NOT NULL);";
		res = sqlite3_exec(m_db, sqlStatement.c_str(), nullptr, nullptr, &errMsg);
		if (res != SQLITE_OK)
		{
			throw std::exception("Failed to create table Attackers");
			return false;
		}

		sqlStatement = "CREATE TABLE AttacksOccurred (MANAGER_USERNAME TEXT NOT NULL, ATTACKER_ID INTEGER NOT NULL, ATTACK_ID INTEGER NOT NULL, ATTACK_DATE DATE NOT NULL, FOREIGN KEY (MANAGER_USERNAME) REFERENCES ManagersInfo(USERNAME), FOREIGN KEY (ATTACKER_ID) REFERENCES AttackersInfo(ID), FOREIGN KEY (ATTACK_ID) REFERENCES AttackTypes(ID));";
		res = sqlite3_exec(m_db, sqlStatement.c_str(), nullptr, nullptr, &errMsg);
		if (res != SQLITE_OK)
		{
			throw std::exception("Failed to create table Attackers");
			return false;
		}
	}
	return true;
}

/*
adds an attack to the db
input:
	std::string attackName- the attack name
output:
	if the attack was inserted successfully
*/
bool SqliteDatabase::addAttack(std::string attackName)
{
	std::string sqlStatement = "";
	char* errMsg = nullptr;

	sqlStatement = "INSERT INTO AttackTypes (ATTACK_NAME) VALUES ('" + attackName + "');";
	int res = sqlite3_exec(m_db, sqlStatement.c_str(), nullptr, nullptr, &errMsg);
	if (res != SQLITE_OK)
	{
		std::cout << errMsg << "\n";
		throw std::exception("Failed to insert an attack");
		return false;
	}
	return true;
}

/*
closes the data base
input:
	none
output:
	none
*/
void SqliteDatabase::close()
{
	sqlite3_close(m_db);
	m_db = nullptr;
}

/*
puts the info of the db about counting
input:
	void* data- general data (not used)
	int argc- the number of columns in the record
	char** argv- the data of the fields
	char** azColName- the name of the columns
output:
	0 if success
*/
int SqliteDatabase::callbackCountObject(void* data, int argc, char** argv, char** azColName)
{
	int i = 0;
	int* tempCounter = (int*)(data);
	for (i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "counter")
		{
			*tempCounter = atoi(argv[i]);
		}
	}
	return 0;
}

/*
adds an attacker to the db
input:
	Client* attacker- the attacker
output:
	none
*/
bool SqliteDatabase::addAttacker(Client* attacker)
{
	std::string sqlStatement = "";
	char* errMsg = nullptr;

	sqlStatement = "INSERT INTO AttackersInfo (IP) VALUES ('" + attacker->getIp().to_string() + "');";
	int res = sqlite3_exec(m_db, sqlStatement.c_str(), nullptr, nullptr, &errMsg);
	if (res != SQLITE_OK)
	{
		std::cout << errMsg << "\n";
		throw std::exception("Failed to insert an attacker");
		return false;
	}
	return true;
}

/*
checks if the client attacked before
input:
	Client* attacker- suspected client for being an attacker
output:
	if the client was found or not
*/
bool SqliteDatabase::doesAttackerExist(Client* attacker)
{
	char* errMsg;
	int tempCounter = 0;

	std::string sqlStatement = "SELECT COUNT(*) AS counter FROM AttackersInfo WHERE IP = '" + attacker->getIp().to_string() + "';";
	int res = sqlite3_exec(m_db, sqlStatement.c_str(), callbackCountObject, (void*)(&tempCounter), &errMsg);
	if (res != SQLITE_OK)
	{
		std::cout << errMsg << "\n";
		throw std::exception("Failed to check if an attacker exists");
	}
	return tempCounter != 0;
}