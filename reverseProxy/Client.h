#ifndef CLIENT
#define CLIENT

#define TINS_STATIC

#include <tins/tins.h>
#include <iostream>
#include <vector>

#include "BruteforceCheck.h"
#include "SlowlorisCheck.h"

using namespace Tins;

class Client {
public:
	Client(IPv4Address ip, int port, std::vector<std::string> urls);
	~Client();

	IPv4Address getIp() const;
	int getPort() const;
	EthernetII getFinPack() const;
	std::vector<IAttackCheck*> getAttackScans() const;

	void setFinPack(PDU& pack);

	bool operator<(Client const& rhs) const;

private:
	IPv4Address m_ip;
	int m_port;
	EthernetII m_finPack;
	std::vector<IAttackCheck*> m_attacks;
};

#endif // !CLIENT
