#include "XssCheck.h"

//constructor for XssCheck class
XssCheck::XssCheck()
{
	std::ifstream file("xssCodes.txt");
	std::string line;

	while (std::getline(file, line)) //push the tags to vector
	{
		this->m_codes.push_back(line);
	}
}

/*
this function will check for xss attacks in the data of the packet
input:
	pack - the packet
output:
	bool - true if there are attcks detected or false if not.
*/
bool XssCheck::check(PDU& pack)
{
	std::string data = convertPayloadToStr(pack.rfind_pdu<RawPDU>());
	std::string line;
	data.erase(remove(data.begin(), data.end(), ' '), data.end());

	for (std::vector<std::string>::iterator it = this->m_codes.begin(); it != this->m_codes.end(); it++)
	{
		line = *it;
		//checks if there is a start and end tag in html
		if (data.find(("<" + line)) != std::string::npos && (data.find(("</>")) != std::string::npos || data.find(("</" + line + ">")) != std::string::npos))
		{
			std::cout << "xss detected: <" + line + ">\n";
			return true;
		}
	}
	return false;
}
