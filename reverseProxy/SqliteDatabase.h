#ifndef SQLITE_DB
#define SQLITE_DB

#include <io.h>
#include <algorithm>
#include <iostream>
#include <vector>
#include "sqlite3.h"
#include "Client.h"

class SqliteDatabase
{
public:
	SqliteDatabase();

	bool open();
	void close();

	bool addAttack(std::string attackName);
	
	bool addAttacker(Client* attacker);

	bool doesAttackerExist(Client* attacker);

private:
	static sqlite3* m_db;

	//callback funcs
	static int callbackCountObject(void* data, int argc, char** argv, char** azColName);
};

#endif // !SQLITE_DB
