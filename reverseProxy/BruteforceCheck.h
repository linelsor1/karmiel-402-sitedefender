#ifndef BRUTEFORCE_CHECK
#define BRUTEFORCE_CHECK

#include "IAttackCheck.h"
#include <chrono>
#include <thread>
#include <map>
#include <vector>

#define MAX_TIME 4000 //in milliseconds
#define MAX_PACKS 100

using namespace std::chrono;

class BruteforceCheck : public IAttackCheck
{
public:
	BruteforceCheck(std::vector<std::string> urls);
	virtual bool check(PDU& pack);
	bool check(PDU& pack, std::string url);


private:
	std::map<std::string, int> m_urlAndSusPacks;
	std::map<std::string, bool> m_urlAndAttack;

	void countTime(std::string url);
};

#endif // !BRUTEFORCE_CHECK
