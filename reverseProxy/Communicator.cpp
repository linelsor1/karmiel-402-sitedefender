#include "Communicator.h"

Communicator::~Communicator()
{
	int i = 0;
	for (i = 0; i < m_clients.size(); i++)
	{
		delete(m_clients[i]);
	}
}

//the func will listen to new users and create a connection with them
void Communicator::startListening()
{
	Sniffer sniffer = configureSniffer("ip dst 127.0.0.1 and tcp dst port 80 and tcp[tcpflags] & (tcp-syn) != 0");

	std::cout << "started listening on port 80...\n\n";

	sniffer.sniff_loop(std::bind(&Communicator::connectClient, this, std::placeholders::_1)); //loops to listen for multiple clients
}

/*
the func will establish the connection with the client and the site
input:
	PDU& clientPack- the client packet
output:
	a boolean if the sniffer should continue
*/
bool Communicator::connectClient(PDU& clientPack)
{
	int numberSameClient = 0; //the counter to see how many times one client is connected
	std::vector<std::string> urls; //demo urls, will be changed to the urls from the UI
	urls.push_back("");
	Client* client = new Client(clientPack.rfind_pdu<IP>().src_addr(), clientPack.rfind_pdu<TCP>().sport(), urls);

	if (!m_db.doesAttackerExist(client)) //if the client didnt attack before
	{
		for (std::vector<Client*>::iterator it = this->m_clients.begin(); it != this->m_clients.end(); it++)
		{
			if ((*it)->getIp() == client->getIp())
			{
				numberSameClient++;
			}
		}
		if (numberSameClient >= 20)
		{
			std::cout << "max connections limit reached for client with ip: " << client->getIp() << std::endl;
			delete client;
		}
		else
		{
			std::string filter = "ip src " + m_siteIP.to_string() + " and tcp and dst port " + std::to_string(client->getPort());
			Sniffer sniffer = configureSniffer(filter);

			sendToSite(clientPack); //send to the site the syn packet

			PDU* synAck = sniffer.next_packet(); //get the answer of the site
			sendToClient(*synAck, client);
			std::thread clientThread(&Communicator::handleClient, this, client); //a thread that will handle the client
			clientThread.detach();
			m_clients.push_back(client); //adds the client to the client list

			std::cout << "\nConnected a client\n";
			std::cout << "Client port is: " << std::to_string(client->getPort()) << "\n";

			delete synAck;
		}
	}
	else
	{
		std::cout << "the client had attacked before!\n";
		delete client;
	}
	return true;
}

/*
the function will handle the client requests
input:
	Client client- the client to handle
output:
	none
*/
void Communicator::handleClient(Client* client)
{
	std::thread t1(&Communicator::snifferFromClient, this, client);
	std::thread t2(&Communicator::snifferFromSite, this, client);
	t1.detach();
	t2.detach();
}

/*
this function will sniff from the site and send all of the packets to the client
input:
	client - the client
output:
	none
*/
void Communicator::snifferFromSite(Client* client)
{
	std::string filterMEssagesToProxy = "ip dst " + m_serverIP.to_string() + " and ";
	std::string filterSniffFromSite = "ip src " + this->m_siteIP.to_string() + " and tcp dst port " + std::to_string(client->getPort());
	Sniffer snifferFromSite = configureSniffer(filterMEssagesToProxy + filterSniffFromSite);
	snifferFromSite.sniff_loop(std::bind(&Communicator::sendToClient, this, std::placeholders::_1, client));
}

/*
this function will sniff from the client and send all of the packets to the site
input:
	client - the client
output:
	none
*/
void Communicator::snifferFromClient(Client* client)
{
	std::string filterMEssagesToProxy = "ip dst 127.0.0.1 and tcp dst port 80 and ";
	std::string filterSniffFromClient = "ip src " + client->getIp().to_string() + " and tcp src port " + std::to_string(client->getPort());
	Sniffer snifferFromClient = configureSniffer(filterMEssagesToProxy + filterSniffFromClient);
	snifferFromClient.sniff_loop(std::bind(&Communicator::scanPacket, this, std::placeholders::_1, client));
}

/*
send to the client the packet from the site
input:
	PDU& packToSend- the packet to send
	Client client- the client to send to
output:
	bool - true, so that the sniff doesn't stop.
*/
bool Communicator::sendToClient(PDU& packToSend, Client* client)
{
	PacketSender sender;
	packToSend.rfind_pdu<IP>().dst_addr(client->getIp());
	packToSend.rfind_pdu<IP>().src_addr("127.0.0.1");

	sender.send(packToSend, m_iface);

	if (packToSend.rfind_pdu<TCP>().get_flag(TCP::FIN)) //the site sent a fin ack and it is the last pack from the site
	{
		return false;
	}
	return true;
}

/*
send to the site the packet from the client
input:
	PDU& packToSend- the packet to send
output:
	bool - so that the sniff doesn't stop
*/
bool Communicator::sendToSite(PDU& packToSend)
{
	PacketSender sender;
	packToSend.rfind_pdu<IP>().src_addr(m_serverIP);
	packToSend.rfind_pdu<IP>().dst_addr(m_siteIP);

	sender.send(packToSend, m_iface);

	return true;
}

/*
the function will send the packet for scanning attacks
input:
	PDU& packToSend- the packet
output:
	none
*/
bool Communicator::scanPacket(PDU& packToSend, Client* client)
{
	int i = 0;
	bool attack = false;
	std::vector<IAttackCheck*> attackScans = client->getAttackScans(); //the unique attack scans
	if (packToSend.find_pdu<RawPDU>())
	{
		std::string payload = packetPayloadToStr(packToSend.rfind_pdu<RawPDU>().payload());
		std::string url = getUrlOfPacket(payload);

		attack = m_xssCheck.check(packToSend);
		if (!attack) //if there is no xss
		{
			attack = m_sqliCheck.check(packToSend);
		}
		for (i = 0; i < attackScans.size() && !attack; i++) //to check every attack in the packet
		{
			if (i == BF) //bruteforce
			{
				BruteforceCheck* bf = (BruteforceCheck*)attackScans[i]; //getting an easier way of referencing the clients bruteorce
				attack = bf->check(packToSend, url);
			}
			else
			{
				attack = attackScans[i]->check(packToSend);
			}
		}
	}
	if (!attack)
	{
		if (packToSend.rfind_pdu<TCP>().get_flag(TCP::FIN)) //the client sent a fin ack and it is the last pack
		{
			client->setFinPack(packToSend); //set the fin pack to discoonect the client
			std::thread t(&Communicator::disconnectClient, this, client);
			t.detach();
			return false; //stop the sniffer
		}
		else
		{
			sendToSite(packToSend);
		}
	}
	else
	{
		client->setFinPack(packToSend);
		disconnectAttacker(client); //disconnect the attacker
		m_db.addAttacker(client); //writes the attacker info in the db
		return false; //stop getting packs from the client
	}
	return true;
}

/*
disconnets a client
input:
	PDU& finPack- the fin package
	Client* client- the attacker to disconnect
output:
	none
*/
void Communicator::disconnectClient(Client* client)
{
	std::cout << "disconnecting...\n";
	std::string filter = "ip src " + m_siteIP.to_string() + " and tcp and dst port " + std::to_string(client->getPort());
	Sniffer sniffer = configureSniffer(filter);

	EthernetII pack = client->getFinPack();

	sendToSite(pack); //send to the site the fin packet
	PDU* finAck = sniffer.next_packet(); //get the answer of the site

	std::string filterMEssagesToProxy = "ip dst 127.0.0.1 and tcp dst port 80 and ";
	std::string filterSniffFromClient = "ip src " + client->getIp().to_string() + " and tcp src port " + std::to_string(client->getPort());
	Sniffer snifferFromClient = configureSniffer(filterMEssagesToProxy + filterSniffFromClient);

	sendToClient(*finAck, client); //send to the client the fin-ack packet
	PDU* ack = snifferFromClient.next_packet(); //get the answer of the client
	sendToSite(*ack);

	delete finAck;
	delete ack;
	std::cout << "disconnected\n";
}

/*
disconnects the attacker from the site after an attack was identified
input:
	PDU& lastPack- the last package that the attacker sent
	Client* client- the attacker to disconnect
output:
	none
*/
void Communicator::disconnectAttacker(Client* client)
{
	std::cout << "disconnecting the attacker...\n";
	std::string filter = "ip src " + m_siteIP.to_string() + " and tcp and dst port " + std::to_string(client->getPort());
	Sniffer sniffer = configureSniffer(filter);

	EthernetII fin = client->getFinPack();

	sendToSite(fin); //send to the site the fin packet
	PDU* finAck = sniffer.next_packet(); //get the answer of the site

	EthernetII eth(fin.rfind_pdu<EthernetII>().dst_addr(), fin.rfind_pdu<EthernetII>().src_addr());

	TCP tcp(80, client->getPort());
	tcp.seq(finAck->rfind_pdu<TCP>().ack_seq());
	tcp.ack_seq(finAck->rfind_pdu<TCP>().seq() + 1);
	tcp.set_flag(TCP::ACK, 1);

	EthernetII ack = eth / IP(m_siteIP, m_serverIP) / tcp;
	sendToSite(ack);
	delete finAck;
	std::cout << "disconnected\n";
}

/*
=======
>>>>>>> b5ec4a1caf8d704e688719614fce735e8236c929
gets the url of the request
input:
	the payload of the packet
output:
	the url
*/
std::string Communicator::getUrlOfPacket(std::string payload)
{
	std::size_t indexOfSubdirectory = payload.find("http://" + m_siteDomain + "/") + (8 + m_siteDomain.size());
	std::string url = "" + payload.substr(indexOfSubdirectory, payload.find(" HTTP/1.1") - indexOfSubdirectory);
	return url;
}

/*
creates the sniffer for the answer
input:
	std::string filter- the filter of the sniffer
output:
	the sniffer
*/
Sniffer Communicator::configureSniffer(std::string filter)
{
	SnifferConfiguration config;
	config.set_filter(filter);
	config.set_immediate_mode(true);
	Sniffer sniffer(m_iface.name(), config); //set the sniffer
	return sniffer;
}

/*
prints the data of the packet
input:
	RawPDU::payload_type& payload- the payload as uint8 vector
output:
	std::string data- the payload as string
*/
std::string Communicator::packetPayloadToStr(RawPDU::payload_type payload)
{
	int i = 0;
	std::string data = "";
	for (i = 0; i < payload.size(); i++)
	{
		data += payload[i];
	}
	return data;
}