#include "SlowlorisCheck.h"

SlowlorisCheck::SlowlorisCheck()
{
	this->isPackRecieved = true; //if we reached the constructor that means a packet was recieved
	this->isAttack = false;
	std::thread t(&SlowlorisCheck::countTime, this);
	t.detach();
}

/*
this function will check for slowloris attacks using the difference in time between messages
input:
	pack - the packet
output:
	bool - true if there are attacks detected or false if not.
*/
bool SlowlorisCheck::check(PDU& pack)
{
	std::string data = convertPayloadToStr(pack.rfind_pdu<RawPDU>());
	std::string line;	
	this->isPackRecieved = true;
	if (this->isAttack)
	{
		std::cout << "slowLoris detected.\n";
	}
	return this->isAttack;
}

/*
this function will check if packs are recieved within a set amount of time. if no packs are recieved it means there is an attack
input:
	none
output:
	none
*/
void SlowlorisCheck::countTime()
{
	while (!isAttack)
	{	
		this->isPackRecieved = false;
		Sleep(WAIT_TIME);// 60 second sleep if the code gets a packet in the mean time var isPackRecieved will be true which means no attack.
		if (this->isPackRecieved == false)
		{
			this->isAttack = true;
		}
		this->isPackRecieved = false;
	}
}